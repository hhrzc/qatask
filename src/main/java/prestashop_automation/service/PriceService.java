package prestashop_automation.service;

import prestashop_automation.model.Currency;
import prestashop_automation.model.Price;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PriceService {

    private PriceService(){}
    private static PriceService instance;

    public static PriceService getInstance(){
        if (instance == null){
            instance = new PriceService();
        }

        return instance;
    }

    //TODO: refactor to SRP
    /**
     * format native string
     * 1) Checking if exist 'priceString' (may be empty or null if passed empty string (if regular price is absent))
     * 2) If 'priceString' exist - normalizes string:
     *      2.1) determines sign and 'Currency' instance
     *      2.2) determines price and change ',' to '.'
     *      2.3) creates BigDecimal instance
     *      2.4) fills and returns 'Price' instance
     * @param priceString - native price string from 'http://prestashop-automation.qatestlab.com.ua/ru/'
     * @return Price instance
     */

    public Price normalizeStringAndGetPrise(String priceString){

        if (priceString == null || priceString.length() < 1){
            return null;
        }

        Currency currency = Currency.getInstanceBySign(priceString.substring(priceString.length() - 1).trim());

        String normalizedPriceString = "";
        Pattern nativeMoneyFormat = Pattern.compile("[0-9]+,[0-9]+");
        Matcher matcher = nativeMoneyFormat.matcher(priceString);
        while (matcher.find()){
            normalizedPriceString = matcher.group().replace(",", ".").trim();
        }

        BigDecimal priceResult = new BigDecimal(normalizedPriceString);

        return new Price().setCurrency(currency).setPrice(priceResult);
    }
}
