package prestashop_automation.model;

public enum SortingType {
    HIGH_TO_LOW,
    LOW_TO_HIGH,
    A_TO_Z,
    Z_TO_A,
    RELEVANT;

    public String getDescription(){
        return
                this == HIGH_TO_LOW ? "order=product.price.desc" :
                        this == LOW_TO_HIGH ? "order=product.price.asc" :
                                this == A_TO_Z ? "order=product.name.asc" :
                                        this == Z_TO_A ? "order=product.name.desc" :
                                                "order=product.position.asc"; //RELEVANT
    }
}