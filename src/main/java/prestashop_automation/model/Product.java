package prestashop_automation.model;

public class Product {

    private String name;
    private Price price;
    private Price regularPrice;
    private boolean discount;
    private int discountPercent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Price getRegularPrice() {
        return regularPrice;
    }

    public int getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(int discountPercent) {
        this.discountPercent = discountPercent;
    }

    /**
     * discount = false if regular price is null
     * @param regularPrice accepts a regular price. If there is no discount for the order,
     *                     the regular price variable is null
     */
    public void setRegularPrice(Price regularPrice) {
        discount = regularPrice != null;
        this.regularPrice = regularPrice;
    }

    public boolean isDiscount() {
        return discount;
    }
}
