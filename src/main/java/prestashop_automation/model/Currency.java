package prestashop_automation.model;

import java.util.Arrays;
import java.util.HashMap;

public class Currency {

    private static HashMap<CurrencyEnums, Currency> cache = new HashMap<>();

    private Currency(){

    }

    private CurrencyEnums currencyEnum;
    private String currencyName;


    private Currency(CurrencyEnums currencyEnum){
        this.currencyEnum = currencyEnum;
        this.currencyName = currencyEnum.name();
    }

    public static Currency getInstance(CurrencyEnums currencyEnums){

        if (cache.containsKey(currencyEnums)){
            return cache.get(currencyEnums);
        }

        Currency result = new Currency(currencyEnums);
        cache.put(currencyEnums, result);

        return result;
    }

    public static Currency getInstanceBySign(String stringWithSign){

        return Arrays.stream(CurrencyEnums.values())
                        .filter(p -> stringWithSign.contains(p.getSign()))
                        .findFirst()
                        .map(Currency::getInstance)
                        .get();
        //TODO: throw exception
    }

    public CurrencyEnums getCurrencyEnum() {
        return currencyEnum;
    }

    public String getCurrencyName() {
        return currencyName;
    }

}
