package tests.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

public class Driver {

    private static WebDriver instance;

    private Driver() {
    }

    public static synchronized WebDriver getInstance(DriversEnum browser) {
        switch (browser) {
            case FIREFOX:
                return instantiateDriver(new FirefoxDriver());
            case CHROME:
                return instantiateDriver(new ChromeDriver());
            case OPERA:
                return instantiateDriver(new OperaDriver());
            default:
                return null;
        }
    }

    private static WebDriver instantiateDriver(WebDriver driver) {
        if (instance == null) {
            instance = driver;
        }
        return instance;
    }
}